import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashbrdComponent } from './dashbrd/dashbrd.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BodymainComponent } from './bodymain/bodymain.component';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    DashbrdComponent,
    HeaderComponent,
    SidebarComponent,
    BodymainComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule
  ],
  exports:[
    DashbrdComponent,
    HeaderComponent,
    SidebarComponent,
    BodymainComponent
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  
})
export class DashboardModule { }
