import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashbrdComponent } from './dashbrd/dashbrd.component';

const routes: Routes = [
  {path:'tboard', component:DashbrdComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
