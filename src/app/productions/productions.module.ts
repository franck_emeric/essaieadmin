import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductionsRoutingModule } from './productions-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProductionsRoutingModule
  ]
})
export class ProductionsModule { }
